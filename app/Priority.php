<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $table = 'tbl_priority';
    protected $primaryKey = 'priority_id';
    public $timestamps = true;
}
