<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::get('/', function () {
    return view('welcome');
});

Route::get('logout', function () {
    return view('logout');
});

Route::get('login', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/task', 'TaskController@index')->name('listalltasks')->middleware('auth');
Route::get('/task/create', 'TaskController@create')->name('task.create')->middleware('auth');
Route::post('/task/store', 'TaskController@store')->name('task.store')->middleware('auth');
Route::get('/task/{task_id}/edit', 'TaskController@edit')->name('task.edit')->middleware('auth');
Route::post('/task/{task_id}/update', 'TaskController@update')->name('task.update')->middleware('auth');
Route::get('/task/{task_id}/delete', 'TaskController@delete')->name('task.delete')->middleware('auth');

Route::get('/user', 'UserController@index')->name('listusers')->middleware('auth');
Route::get('/user/create', 'UserController@create')->name('user.create')->middleware('auth');
Route::post('/user/store', 'UserController@store')->name('user.store')->middleware('auth');
Route::get('/user/{user_id}/edit', 'UserController@edit')->name('user.edit')->middleware('auth');
Route::post('/user/{user_id}/update', 'UserController@update')->name('user.update')->middleware('auth');
Route::get('/user/{user_id}/delete', 'UserController@delete')->name('user.delete')->middleware('auth');
Route::get('/user/{token}/confirm', 'ConfirmRegisterController@confirm')->name('user.confirm');
            