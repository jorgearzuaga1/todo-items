<?php

namespace App;

/**
 * This class has generals functions
 * @author jorge
 *
 */
class GeneralFunctions
{
    
    public static function showFlashMessages($arrMessages){
        $messages = '';
        if (count($arrMessages)>0){
            foreach ($arrMessages as $typeMessage => $arrTypesMessages){ 
                $messages .= '<div class="alert alert-'.$typeMessage.'">';                
                $messages .= '<ul>';
                if(count($arrTypesMessages) > 0){
                    foreach ($arrTypesMessages as $message){
                        $messages .= '<li>'.$message.'</li>';
                    }
                }
                $messages .= '</ul>';
                $messages .= '</div>';
            }           
        }
        return $messages;        
    }
    
}
