<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $table = 'tbl_tasks';

    protected $primaryKey = 'task_id';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'priority_id',
        'due_date',
        'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'task_id'
    ];

    public static $rules = array(
        'name' => 'required|string|max:255',
        'due_date' => 'required|date',
        'priority_id' => 'required|int',
        'user_id' => 'required|int'
    );

    public function priority()
    {
        return $this->hasOne(Priority::class, 'priority_id');
    }
}
