<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Task</div>
                   <div class="flash-alerts">
                   	 <?= $message ?>
                    </div>
                   <?php if(!$error){ ?> 
                <div class="panel-body">
                  <?= Form::model($task, ['id' => 'taskForm','route' => ['task.update', $task->task_id], 'method'=>'post']) ?>
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
								<?= Form::text('name', $task->name); ?>
                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('due_date') ? ' has-error' : ''); ?>">
                            <label for="due_date" class="col-md-4 control-label">Due Date</label>

                            <div class="col-md-6">
                                <?= Form::date('due_date', $task->due_date, ['class'=>'datepicker']); ?>
                                <?php if($errors->has('due_date')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('due_date')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('priority_id') ? ' has-error' : ''); ?>">
                            <label for="due_date" class="col-md-4 control-label">Priority</label>

                            <div class="col-md-6">
                            	<?= Form::select('priority_id', $prioritys, null, ['placeholder' => 'Select a Priority...']); ?>
                                
                                <?php if($errors->has('priority_id')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('priority_id')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Update
                                </button>
                            </div>
                        </div>
                        
                        <?= Form::close() ?>
                </div>
                 <?php } ?> 
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(asset('js/task.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>