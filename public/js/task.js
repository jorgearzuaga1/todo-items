$(function() {
	$('.datepicker').datepicker({
		format : "yyyy-mm-dd",
		autoclose : true
	});

	$('#taskForm').on('submit', function(e) {
		$.ajaxSetup({
			header : $('meta[name="_token"]').attr('content')
		})
		e.preventDefault(e);
		$.ajax({
			type : "POST",
			url : $(this).attr('action'),
			data : $(this).serialize(),
			dataType : 'json',
			success : function(data) {
				if (data.message != '') {
					$('.flash-alerts').html(data.message);
					if (!data.error) {
						setTimeout(location.reload(), 5000);
					}
				}
			},
			error : function(data) {
				$('.flash-alerts').html(data);
			}
		})
	});

});
