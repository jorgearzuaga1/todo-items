<?php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\GeneralFunctions;

class ConfirmRegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'token' => 'required',
            'token_expire' => 'required'
        ]);
    }
    /**
     * Confirm the register for a new user
     * @param string $token
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function confirm($token)
    {
        $user = User::where('token', '=', trim($token))->first();
        if ($user) {
            $dateExp = new \DateTime($user->token_expire);
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $diffTimeStamp = $dateNow->getTimestamp() - $dateExp->getTimestamp();
            $minutesToExp = round($diffTimeStamp / 60);
            if ($minutesToExp <= config('app.token_expire')) {
                $user->token = null;
                $user->token_expire = null;
                $user->save();
                $flashMessage['success'][] = 'Successfully updated user!';
            } else {
                $flashMessage['danger'][] = 'Token expired!';
            }
        } else {
            $flashMessage['danger'][] = 'Token invalid!';
        }        
        $message = GeneralFunctions::showFlashMessages($flashMessage);
        return view('user.confirm', array('message' => $message));
    }
}
