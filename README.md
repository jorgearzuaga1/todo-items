Instrucciones:

1. Clone el repositorio o descomprima el repositorio en su carpeta www.
2. Crear la base de datos MySQL todo-items.
3. Ejecute el script que se encuentra en la raíz todo-items.sql.
4. Configure el archivo .env de la raíz y coloque el usuario y password del MySQL:
	DB_USERNAME=root
	DB_PASSWORD=123456
5. Ingrese a http://localhost/todo-items con:
	usuario: jorgearzuaga1@gmail.com
	contraseña: 123456
	

