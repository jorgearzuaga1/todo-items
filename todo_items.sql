/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : todo_items

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-08-09 13:25:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_08_03_192016_create_tasks_table', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_priority
-- ----------------------------
DROP TABLE IF EXISTS `tbl_priority`;
CREATE TABLE `tbl_priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` varchar(50) NOT NULL,
  `active` int(1) DEFAULT '1' COMMENT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_priority
-- ----------------------------
INSERT INTO `tbl_priority` VALUES ('1', 'Low', '1', null, null);
INSERT INTO `tbl_priority` VALUES ('2', 'Medium', '1', null, null);
INSERT INTO `tbl_priority` VALUES ('3', 'High', '1', null, null);

-- ----------------------------
-- Table structure for tbl_tasks
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tasks`;
CREATE TABLE `tbl_tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `user_id` (`user_id`),
  KEY `priority_id` (`priority_id`),
  CONSTRAINT `tbl_tasks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tbl_tasks_ibfk_2` FOREIGN KEY (`priority_id`) REFERENCES `tbl_priority` (`priority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_tasks
-- ----------------------------
INSERT INTO `tbl_tasks` VALUES ('7', '35', 'First Task2', '2', '2017-08-09', '2017-08-08 15:56:04', '2017-08-08 19:38:23');
INSERT INTO `tbl_tasks` VALUES ('8', '35', 'First Task1', '2', '2017-08-09', '2017-08-08 15:56:19', '2017-08-08 15:56:19');
INSERT INTO `tbl_tasks` VALUES ('10', '35', 'admin2', '3', '2017-08-17', '2017-08-08 19:40:10', '2017-08-09 14:47:46');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expire` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Jorge', 'jorgearzuaga1@gmail.com', '$2y$10$HEkIdRtYlFQeOHG233n5heJyKx5wuZYkak2XTA9JW1oRjLNOoo43m', 'fMVYPUKW9RETutpGijx4346U6qanvaI6wFks5uKbnPZdduvDFlV4MHrOE6MN', null, '2017-08-09 10:53:48', '2017-08-03 21:06:06', '2017-08-03 21:06:06');
INSERT INTO `users` VALUES ('35', 'AA', 'jorgearzuaga2@gmail.com', '$2y$10$YaB8cqbKi6uiSvcdm0oYEuda6whg.w/3KflwmxYXjCuW8cLcW1yja', 'fl8crRenS01sHf6ORFXZeMG1ZIyQ4jGxhmlWDSJwzNxfruEOlspNdio8jzgu', 'c442a2e41fa1c00b9943e4574c6fd82c5e3de377', '2017-08-09 10:35:02', '2017-08-09 13:41:04', '2017-08-09 13:41:04');
SET FOREIGN_KEY_CHECKS=1;
