<style>
.container {
	width: 100%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: center;
}

.container-button {
	width: 50%;
	margin-left: 25%
}

h1 {
	word-break: normal;
	line-height: 30px;
	font-size: 28px;
	font-weight: bold;
	padding-bottom: 40px;
	margin: 0
}
</style>
<div class="container">

	<h1
		style="word-break: normal; line-height: 30px; font-size: 28px; font-weight: bold; padding-bottom: 40px; margin: 0">
		Welcome <br>
		<span style="font-size: 23px; font-weight: normal"><?= $user->email ?>!</span>
	</h1>
	<p class="m_3303252136584788631lead"
		style="font-size: 23px; font-weight: normal; line-height: 30px; padding-bottom: 40px; margin: 0">Thank
		you for signing up!</p>
	<p class="m_3303252136584788631lead"
		style="font-size: 23px; font-weight: normal; line-height: 30px; padding-bottom: 40px; margin: 0">Please
		verify Your Email Address by clicking the link below!</p>

	<div class="container-button">
		<table border="0" cellpadding="0" cellspacing="0" width="335"
			class="m_3303252136584788631button-block"
			style="border-spacing: 0; border-collapse: separate; table-layout: auto; width: 335px; padding: 0">
			<tbody>
				<tr style="padding: 0">
					<td align="center" valign="middle"
						class="m_3303252136584788631button"
						style="word-break: break-word; border-collapse: collapse !important; border-radius: 35px; padding: 20px 25px"
						bgcolor="#00c9c9"><a
						href="http://localhost/todo-items/public/user/<?= $user->token ?>/confirm"
						style="color: #fff !important; text-decoration: none; display: block; font-size: 23px; font-style: italic"
						target="_blank">Confirm </a></td>
				</tr>
			</tbody>
		</table>

	</div>
</div>