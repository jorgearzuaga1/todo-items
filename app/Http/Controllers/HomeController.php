<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $taskQuery = Task::select('tbl_tasks.*')->whereBetween(\DB::raw('DATEDIFF(tbl_tasks.due_date,now())'), [
            0,
            config('app.task_expire')
        ])
            ->where('tbl_tasks.user_id', '=', $user->id)
            ->get();
        
        return view('home', array('taskQuery' => $taskQuery));
    }
}
