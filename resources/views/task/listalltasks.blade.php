@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    @if(session()->has('message.level'))
        <div class="alert alert-{{ session('message.level') }}"> 
        {!! session('message.content') !!}
        </div>
    @endif
        {!! $grid !!}
    </div>
</div>
<script src="{{ asset('js/task.js') }}"></script>
@endsection
