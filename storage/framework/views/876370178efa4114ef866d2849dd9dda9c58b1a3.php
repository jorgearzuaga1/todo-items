<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
    <?php if(session()->has('message.level')): ?>
        <div class="alert alert-<?php echo e(session('message.level')); ?>"> 
        <?php echo session('message.content'); ?>

        </div>
    <?php endif; ?>
        <?php echo $grid; ?>

    </div>
</div>
<script src="<?php echo e(asset('js/user.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>