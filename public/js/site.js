$(function() {
	$('.datepicker').datepicker({
		format : "yyyy-mm-dd",
		autoclose : true
	});

	$('.edit-task').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.show({
			title : 'Update Task',
			message : $('<div></div>').load(url)
		});
	});

	$('.new-task').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.show({
			title : 'Create Task',
			message : $('<div></div>').load(url)
		});
	});

	$('.delete-task').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.confirm({
			title : 'WARNING',
			message : 'Warning! Delete this task?',
			type : BootstrapDialog.TYPE_WARNING,
			closable : true,
			draggable : false,
			btnCancelLabel : 'Cancel',
			btnOKLabel : 'Delete',
			btnOKClass : 'btn-warning',
			callback : function(result) {
				if (result) {
					$.ajax({
						type : "GET",
						url : url,
						dataType : 'json',
						success : function(data) {
							if (data.error == false) {																
								location.reload();
								return true;
							}else{
								alert(data.message);
								return false;
							}
						}
					})
					return true;
				} else {
					return false;
				}
			}
		});

	});

	$('.edit-user').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.show({
			title : 'Update User',
			message : $('<div></div>').load(url)
		});
	});

	$('.new-user').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.show({
			title : 'Create User',
			message : $('<div></div>').load(url)
		});
	});

	$('.delete-user').click(function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		BootstrapDialog.confirm({
			title : 'WARNING',
			message : 'Warning! Delete this user?',
			type : BootstrapDialog.TYPE_WARNING,
			closable : true,
			draggable : false,
			btnCancelLabel : 'Cancel',
			btnOKLabel : 'Delete',
			btnOKClass : 'btn-warning',
			callback : function(result) {
				if (result) {
					$.ajax({
						type : "GET",
						url : url,
						dataType : 'json',
						success : function(data) {
							if (data.error == false) {															
								location.reload();
								return true;
							}else{
								alert(data.message);
								return false;
							}
						}
					})
				} else {
					return false;
				}
			}
		});

	});

});
