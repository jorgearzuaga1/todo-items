 <?php $__env->startSection('content'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Dashboard</div>

				<div class="panel-body">You are logged in!</div>
			</div>
		</div>
	</div>
	<?php  
	if(count($taskQuery) > 0){ ?>
	<div class="alert alert-danger" role="alert">These tasks are about to
		expire</div>
		<div class="row">	
        			<?php 
        			$i = 1;
        			foreach ($taskQuery as $reg) { ?>                    
                        <div class="col-md-3 div-task">
                        Name: <?= $reg->name; ?><br>
                        Due Date: <?= $reg->due_date; ?><br>
                        </div>
                    <?php if($i == 3){ $i=0; ?>
                     	</div>
                    	<div class="row" style="margin-top: 10px">	
                    <?php }
                	   $i++;
        			} 
        			?>
        </div>
        <?php } ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>