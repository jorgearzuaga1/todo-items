<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Task;
use Grids;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use App\Priority;
use App\GeneralFunctions;

class TaskController extends Controller
{

    /**
     * Show all task for an loged user in a grid
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        // Get the currently authenticated user...
        $user = Auth::user();
        
        $query = Task::leftJoin('tbl_priority', 'tbl_priority.priority_id', '=', 'tbl_tasks.priority_id')->select('tbl_tasks.*')
            ->addSelect('tbl_priority.priority as priority')
            ->where('tbl_tasks.user_id', '=', $user->id);
        
        $grid = new Grid((new GridConfig())->
        // Grids name used as html id, caching key, filtering GET params prefix, etc
        // If not specified, unique value based on file name & line of code will be generated
        setName('my_report')
            ->
        // See all supported data providers in sources
        setDataProvider(new EloquentDataProvider($query))
            ->setPageSize(5)
            ->setCachingTime(0)
            ->
        // Setup table columns
        setColumns([
            (new FieldConfig())->setName('task_id')
                ->setLabel('Task Id')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            (new FieldConfig())->setName('due_date')
                ->setLabel('Due Date')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            (new FieldConfig())->setName('name')
                ->setLabel('Task Name')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            (new FieldConfig())->setName('priority')
                ->setLabel('Priority')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            
            (new FieldConfig())->setName('task_id')
                ->setLabel('Actions')
                ->setCallback(function ($val) {
                $iconEdit = '<span href="task/' . $val . '/edit" class="cursor glyphicon glyphicon-edit edit-task"></span>&nbsp;';
                $iconDelete = '<span href="task/' . $val . '/delete" class="cursor glyphicon glyphicon-trash delete-task"></span>&nbsp;';
                return '<small>' . $iconEdit . $iconDelete . '</small>';
            })
        ]));
        return view('task.listalltasks', array(
            'grid' => $grid
        ));
    }

    /**
     * Create a new task
     *
     * @param Request $request
     */
    protected function store(Request $request)
    {
        $user = Auth::user();
        $request['user_id'] = $user->id;
        
        $validator = Validator::make(\Input::all(), Task::$rules);
        $message = '';
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->any()) {
                $flashMessage['danger'] = $errors->all();
            }
            $response['error'] = true;
        } else {
            // store
            $task = new Task();
            $task->name = \Input::get('name');
            $task->due_date = \Input::get('due_date');
            $task->priority_id = \Input::get('priority_id');
            $task->user_id = $user->id;
            $task->save();
            $response['error'] = false;
            $flashMessage['success'][] = 'Successfully created task!';
        }
        $response['message'] = GeneralFunctions::showFlashMessages($flashMessage);
        echo json_encode($response);
    }

    /**
     * Display form to create new task
     */
    protected function create()
    {
        $task = new Task();
        $prioritys = Priority::where('active', '=', 1)->pluck('priority', 'priority_id');
        
        return view('task.create', array(
            'task' => $task,
            'prioritys' => $prioritys
        ));
    }
    /**
     * Display form to edit new task
     * 
     * @param int $task_id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function edit($task_id = null)
    {
        $user = Auth::user();
        $task = Task::find($task_id);
        $error = false;
        $message = '';
        if (is_null($task) || $user->id != $task->user_id) {
            $error = true;
            $flashMessage['danger'][] = 'Task not found!';
            $message = GeneralFunctions::showFlashMessages($flashMessage);
        }
        
        $prioritys = Priority::where('active', '=', 1)->pluck('priority', 'priority_id');        
        return view('task.edit', array(
            'task' => $task,
            'prioritys' => $prioritys,
            'error' => $error,
            'message' => $message
        ));
    }

    /**
     * Update a task after
     * 
     * @param int $task_id
     * @param Request $request
     */
    protected function update($task_id, Request $request)
    {
        $user = Auth::user();
        $request['user_id'] = $user->id;
        
        $validator = Validator::make(\Input::all(), Task::$rules);
        $message = '';
        $flashMessage = array();
        if ($validator->fails()) {
            $errors = $validator->errors();
            $flashMessage['danger'] = $errors->all();
            $response['error'] = true;
        } else {
            // store
            $task = Task::find($task_id);
            if ($task && $user->id == $task->user_id) {
                $task->name = \Input::get('name');
                $task->due_date = \Input::get('due_date');
                $task->priority_id = \Input::get('priority_id');
                $task->save();
                $response['error'] = false;
                $flashMessage['success'][] = 'Successfully updated task!';  
            }else{
                $response ['error'] = true;
                $flashMessage['danger'][] = 'Task not found!';  
            }
        }        
        $response ['message'] = GeneralFunctions::showFlashMessages($flashMessage);
        echo json_encode ( $response );
    }    
    
    /**
     *  Delete a task    
     * 
     * @param int $task_id
     */
    protected function delete($task_id = null)
    {
        $task = Task::find($task_id);  
        $user = Auth::user();
        if($task && $user->id == $task->user_id){
            try {
                if($task->delete()){
                    $response ['error'] = false;
                }
            } catch (\Exception $e) {
                $response ['error'] = true;
                $response ['message'] = 'This task can not be deleted!';
            }
        }else{
            $response ['error'] = true;
            $response ['message'] = 'Task not found!';
        }
        echo json_encode ( $response );
    }
}
