<?php
namespace App\Http\Controllers;

use App\User;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Grids;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use App\Priority;
use App\GeneralFunctions;
use Ramsey\Uuid\Codec\TimestampFirstCombCodec;

class UserController extends Controller
{
    /**
     * Show all users in a grid
     * 
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        // Get the currently authenticated user...
        $query = User::select('*');
        
        $grid = new Grid((new GridConfig())->
        // Grids name used as html id, caching key, filtering GET params prefix, etc
        // If not specified, unique value based on file name & line of code will be generated
        setName('my_report')
            ->
        // See all supported data providers in sources
        setDataProvider(new EloquentDataProvider($query))
            ->setPageSize(5)
            ->setCachingTime(0)
            ->
        // Setup table columns
        setColumns([
            (new FieldConfig())->setName('id')
                ->setLabel('User Id')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            (new FieldConfig())->setName('name')
                ->setLabel('Name')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            (new FieldConfig())->setName('email')
                ->setLabel('Email')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            
            (new FieldConfig())->setName('token')
                ->setLabel('Token')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            
            (new FieldConfig())->setName('token_expire')
                ->setLabel('Token Expire')
                ->setSortable(true)
                ->addFilter((new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)),
            
            (new FieldConfig())->setName('id')
                ->setLabel('Actions')
                ->setCallback(function ($val) {
                $iconDelete = '<span href="user/' . $val . '/delete" class="cursor glyphicon glyphicon-trash delete-user"></span>&nbsp;';
                return '<small>'  . $iconDelete . '</small>';
            })
        ]));
        return view('user.listusers', array(
            'grid' => $grid
        ));
    }

    /**
     * Create a new user.
     *
     * @param Request $request
     */
    protected function store(Request $request)
    {
        $validator = Validator::make(\Input::all(), User::$rules);
        $message = '';
        $flashMessage = array();
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->any()) {
                $flashMessage['danger'] = $errors->all();
            }
            $response['error'] = true;
        } else {
            // store
            $user = User::where('email', '=', \Input::get('email'))->first();
            if (! $user) {
                try {
                    $date = new \DateTime();
                    $date->modify('+60 minutes');
                    $user = new User();
                    $user->name = \Input::get('name');
                    $user->email = \Input::get('email');
                    $user->password = bcrypt(\Input::get('password'));
                    $user->token = sha1(str_random(10));
                    $user->token_expire = $date->format('Y-m-d H:i:s');
                    $user->save();
                    $response['error'] = false;
                    try {
                        $mailSended = Mail::send('user.emailregister', [
                            'user' => $user
                        ], function ($m) use ($user) {
                            $m->to($user->email, $user->name)->subject('User created!');
                        });
                    } catch (\Exception $e) {
                        $response['error'] = true;
                        $flashMessage['danger'][] = 'Mail not sended! <br>' . $e->getMessage();
                    }
                    $flashMessage['success'][] = 'Successfully created user!';
                } catch (\Exception $e) {
                    $response['error'] = true;
                    $flashMessage['danger'][] = 'This email was registered, try another!';
                }
            } else {
                $response['error'] = true;
                $flashMessage['danger'][] = 'This email was registered, try another!';
            }
        }
        $response['message'] = GeneralFunctions::showFlashMessages($flashMessage);
        echo json_encode($response);
    }

    /**
     * Display form to create an user
     */
    protected function create()
    {
        $user = new User();
        
        return view('user.create', array(
            'user' => $user
        ));
    }

    /**
     * Display form to edit user
     */
    protected function edit($user_id = null)
    {
        $user = User::find($user_id);
        if (is_null($user)) {
            return \Redirect::route('user.index');
        }
        
        return view('user.edit', array(
            'user' => $user
        ));
    }   
    /**
     * Update an user after
     * 
     * @param int $user_id
     * @param Request $request
     */
    protected function update($user_id, Request $request)
    {
        $validator = Validator::make(\Input::all(), User::$rules);
        $message = '';
        $flashMessage = array();
        if ($validator->fails()) {
            $errors = $validator->errors();
            $flashMessage['danger'] = $errors->all();
            $response['error'] = true;
        } else {
            // store
            try {
                $user = User::find($user_id);
                $user->name = \Input::get('name');
                $user->email = \Input::get('email');
                $user->password = bcrypt(\Input::get('password'));
                $user->save();
                $response['error'] = false;
                $flashMessage['success'][] = 'Successfully updated user!';                
            } catch (\Exception $e) {
                $response ['error'] = true;
                $flashMessage['danger'][] = 'This email was registered, try another!';  
            }
            
        }
        $response ['message'] = GeneralFunctions::showFlashMessages($flashMessage);
        echo json_encode ( $response );
    }
    
    /**
     *  Delete an user
     *
     *  @param int $user_id
     */
    protected function delete($user_id = null)
    {
        $user = User::find($user_id);
        if($user){
            try {            
                if($user->delete()){
                    $response ['error'] = false;
                }
            } catch (\Exception $e) {
                $response ['error'] = true;
                $response ['message'] = 'This user can not be deleted, has associated tasks!';
            }
        }else{
            $response ['error'] = true;
            $response ['message'] = 'User not found!';
        }
        echo json_encode ( $response );
    }
}
